runconf = configuration_data()
runconf.set('bindir', bindir)
runconf.set('libexecdir', libexecdir)
runconf.set('pkgdatadir', pkgdatadir)
runconf.set('version', meson.project_version())
runconf.set('compositor', get_option('compositor'))

configure_file(
  input: 'phog.in',
  output: 'phog',
  install_dir: bindir,
  configuration: runconf,
  install: true
)

schemasdir = 'share/glib-2.0/schemas'

#generate XML enum definitions for GSettings schema
schema_enum_headers = files(
  '../src/wwan/phog-wwan-backend.h',
)
generate_enums_schema = gnome.mkenums('org.mobian.phog.enums.xml',
  sources: schema_enum_headers,
  comments: '<!-- @comment@ -->',
  fhead: '<schemalist>',
  vhead: '  <@type@ id="org.mobian.phog.@EnumName@">',
  vprod: '    <value nick="@valuenick@" value="@valuenum@"/>',
  vtail: '  </@type@>',
  ftail: '</schemalist>',
  install_header: true,
  install_dir: schemasdir
)

#workaround due to https://github.com/mesonbuild/meson/issues/1687
copy_schema = custom_target('copy-gschema-to-builddir',
  input: 'org.mobian.phog.gschema.xml',
  output: 'org.mobian.phog.gschema.xml',
  command: ['cp', '@INPUT@', '@OUTPUT@']
)
schemas = ['org.mobian.phog.gschema.xml']
compile_schemas = custom_target('glib-compile-schemas',
  build_by_default: true,
  output: 'gschemas.compiled',
  install: false,
  command: [find_program('glib-compile-schemas'),
    meson.current_build_dir()
  ],
  depends: [generate_enums_schema, copy_schema]
)

install_data(
  schemas,
  install_dir: schemasdir
)

install_data('phoc.ini', install_dir : pkgdatadir)
