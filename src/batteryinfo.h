/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "status-icon.h"

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define PHOG_TYPE_BATTERY_INFO (phog_battery_info_get_type())

G_DECLARE_FINAL_TYPE (PhogBatteryInfo, phog_battery_info, PHOG, BATTERY_INFO, PhogStatusIcon)

GtkWidget * phog_battery_info_new (void);

G_END_DECLS
