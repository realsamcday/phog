/*
 * Copyright (C) 2022 Collabora Ltd
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Arnaud Ferraris <arnaud.ferraris@collabora.com>
 */

#pragma once

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

G_BEGIN_DECLS

#define PHOG_TYPE_GREETD (phog_greetd_get_type ())

G_DECLARE_FINAL_TYPE (PhogGreetd, phog_greetd, PHOG, GREETD,
                      GObject)

GObject            *phog_greetd_new (void);

GHashTable         *phog_greetd_get_available_users (PhogGreetd *self);
GListStore         *phog_greetd_get_available_sessions (PhogGreetd *self);
guint               phog_greetd_get_last_session_idx (PhogGreetd *self);

gboolean            phog_greetd_create_session (PhogGreetd *self, const gchar * user, GError **error);
gboolean            phog_greetd_authenticate (PhogGreetd *self, const gchar * password, GError **error);
gboolean            phog_greetd_start_session (PhogGreetd *self, gint session_id, GError **error);
gboolean            phog_greetd_cancel_session (PhogGreetd *self, GError **error);

G_END_DECLS
