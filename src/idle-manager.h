/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */
#pragma once

#include <glib-object.h>
#include "phog-idle-dbus.h"

#define PHOG_TYPE_IDLE_MANAGER                 (phog_idle_manager_get_type ())
G_DECLARE_FINAL_TYPE (PhogIdleManager, phog_idle_manager, PHOG, IDLE_MANAGER, GObject)

PhogIdleManager * phog_idle_manager_get_default    (void);
void phog_idle_manager_reset_timers (PhogIdleManager *self);
