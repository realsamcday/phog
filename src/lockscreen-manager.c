/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#define G_LOG_DOMAIN "phog-lockscreen-manager"

#include "lockscreen-manager.h"
#include "lockscreen.h"
#include "lockshield.h"
#include "monitor-manager.h"
#include "monitor/monitor.h"
#include "phog-wayland.h"
#include "shell.h"
#include "util.h"
#include <gdk/gdkwayland.h>

/**
 * SECTION:lockscreen-manager
 * @short_description: The singleton that manages screen locking
 * @Title: PhogLockscreenManager
 *
 * The #PhogLockscreenManager is responsible for putting the #PhogLockscreen
 * on the primary output and a #PhogLockshield on other outputs when the session
 * becomes idle or when invoked explicitly via phog_lockscreen_manager_set_locked().
 */

enum {
  WAKEUP_OUTPUTS,
  N_SIGNALS
};
static guint signals[N_SIGNALS] = { 0 };

enum {
  PROP_0,
  PROP_LOCKED,
  PROP_LAST_PROP
};
static GParamSpec *props[PROP_LAST_PROP];


struct _PhogLockscreenManager {
  GObject parent;

  PhogLockscreen      *lockscreen;     /* phone display lock screen */
  GPtrArray             *shields;       /* other outputs */

  gboolean locked;
  gint64 active_time;                   /* when lock was activated (in us) */
  int transform;                        /* the shell transform before locking */
};

G_DEFINE_TYPE (PhogLockscreenManager, phog_lockscreen_manager, G_TYPE_OBJECT)


static void
lockscreen_unlock_cb (PhogLockscreenManager *self, PhogLockscreen *lockscreen)
{
  PhogShell *shell = phog_shell_get_default ();
  PhogMonitorManager *monitor_manager = phog_shell_get_monitor_manager (shell);
  PhogMonitor *primary_monitor = phog_shell_get_primary_monitor (shell);

  g_return_if_fail (PHOG_IS_LOCKSCREEN (lockscreen));
  g_return_if_fail (lockscreen == PHOG_LOCKSCREEN (self->lockscreen));

  g_signal_handlers_disconnect_by_data (monitor_manager, self);
  g_signal_handlers_disconnect_by_data (primary_monitor, self);
  g_signal_handlers_disconnect_by_data (shell, self);
  g_clear_pointer (&self->lockscreen, phog_cp_widget_destroy);

  /* Unlock all other outputs */
  g_clear_pointer (&self->shields, g_ptr_array_unref);

  self->locked = FALSE;
  self->active_time = 0;
  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_LOCKED]);
}


static void
lockscreen_wakeup_output_cb (PhogLockscreenManager *self, PhogLockscreen *lockscreen)
{
  g_return_if_fail (PHOG_IS_LOCKSCREEN_MANAGER (self));
  g_return_if_fail (PHOG_IS_LOCKSCREEN (lockscreen));

  /* we just proxy the signal here */
  g_signal_emit (self, signals[WAKEUP_OUTPUTS], 0);
}


/* Lock a non primary monitor bringing up a shield */
static void
lock_monitor (PhogLockscreenManager *self,
              PhogMonitor           *monitor)
{
  PhogShell *shell = phog_shell_get_default ();
  PhogWayland *wl = phog_wayland_get_default ();
  GtkWidget *shield;

  /* Primary monitor is handled via on_primary_monitor_changed */
  if (phog_shell_get_primary_monitor (shell) == monitor)
    return;

  g_debug ("Adding shield for %s", monitor->name);
  shield = phog_lockshield_new (
    phog_wayland_get_zwlr_layer_shell_v1 (wl),
    monitor->wl_output);

  g_object_set_data (G_OBJECT (shield), "phog-monitor", monitor);

  g_ptr_array_add (self->shields, shield);
  gtk_widget_show (shield);
}


static void
remove_shield_by_monitor (PhogLockscreenManager *self,
                          PhogMonitor           *monitor)
{
  for (int i = 0; i < self->shields->len; i++) {
    PhogMonitor *shield_monitor;
    PhogLockshield *shield = g_ptr_array_index (self->shields, i);

    shield_monitor = g_object_get_data (G_OBJECT (shield), "phog-monitor");
    g_return_if_fail (PHOG_IS_MONITOR (shield_monitor));
    if (shield_monitor == monitor) {
      g_debug ("Removing shield %p", shield);
      g_ptr_array_remove (self->shields, shield);
      break;
    }
  }
}


static void
on_monitor_removed (PhogLockscreenManager *self,
                    PhogMonitor           *monitor,
                    PhogMonitorManager    *monitormanager)
{


  g_return_if_fail (PHOG_IS_MONITOR (monitor));
  g_return_if_fail (PHOG_IS_LOCKSCREEN_MANAGER (self));

  g_debug ("Monitor '%s' removed", monitor->name);
  remove_shield_by_monitor (self, monitor);
}


static void
on_monitor_added (PhogLockscreenManager *self,
                  PhogMonitor           *monitor,
                  PhogMonitorManager    *monitormanager)
{
  g_return_if_fail (PHOG_IS_MONITOR (monitor));
  g_return_if_fail (PHOG_IS_LOCKSCREEN_MANAGER (self));

  g_debug ("Monitor '%s' added", monitor->name);
  lock_monitor (self, monitor);
}


static void
lock_primary_monitor (PhogLockscreenManager *self)
{
  PhogMonitor *primary_monitor;
  PhogWayland *wl = phog_wayland_get_default ();
  PhogShell *shell = phog_shell_get_default ();

  primary_monitor = phog_shell_get_primary_monitor (shell);
  g_assert (primary_monitor);

  /* The primary output gets the clock, keypad, ... */
  self->lockscreen = PHOG_LOCKSCREEN (phog_lockscreen_new (
                                         phog_wayland_get_zwlr_layer_shell_v1 (wl),
                                         primary_monitor->wl_output));
  g_object_connect (
    self->lockscreen,
    "swapped-object-signal::lockscreen-unlock", G_CALLBACK (lockscreen_unlock_cb), self,
    "swapped-object-signal::wakeup-output", G_CALLBACK (lockscreen_wakeup_output_cb), self,
    NULL);

  gtk_widget_show (GTK_WIDGET (self->lockscreen));
  /* Old lockscreen gets remove due to `layer_surface_closed` */
}


static void
on_primary_monitor_changed (PhogLockscreenManager *self,
                            GParamSpec *pspec,
                            PhogShell *shell)
{
  PhogMonitor *monitor;

  g_return_if_fail (PHOG_IS_SHELL (shell));
  g_return_if_fail (PHOG_IS_LOCKSCREEN_MANAGER (self));

  monitor = phog_shell_get_primary_monitor (shell);

  if (monitor) {
    g_debug ("primary monitor changed to %s, need to move lockscreen", monitor->name);
    lock_primary_monitor (self);
    /* We don't remove a shield that might exist to avoid the screen
       content flickering in. The shield will be removed on unlock */
  } else {
    g_debug ("Primary monitor gone, doing nothing");
  }
}


static void
lockscreen_lock (PhogLockscreenManager *self)
{
  PhogMonitor *primary_monitor;
  PhogShell *shell = phog_shell_get_default ();
  PhogMonitorManager *monitor_manager = phog_shell_get_monitor_manager (shell);

  g_return_if_fail (!self->locked);

  primary_monitor = phog_shell_get_primary_monitor (shell);

  /* Listen for monitor changes */
  g_signal_connect_object (monitor_manager, "monitor-added",
                           G_CALLBACK (on_monitor_added),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (monitor_manager, "monitor-removed",
                           G_CALLBACK (on_monitor_removed),
                           self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (shell,
                           "notify::primary-monitor",
                           G_CALLBACK (on_primary_monitor_changed),
                           self,
                           G_CONNECT_SWAPPED);

  if (primary_monitor)
    lock_primary_monitor (self);
  else
    g_message ("No primary monitor to lock");

  /* Lock all other outputs */
  self->shields = g_ptr_array_new_with_free_func ((GDestroyNotify) (gtk_widget_destroy));
  for (int i = 0; i < phog_monitor_manager_get_num_monitors (monitor_manager); i++) {
    PhogMonitor *monitor = phog_monitor_manager_get_monitor (monitor_manager, i);

    if (monitor == NULL || monitor == primary_monitor)
      continue;
    lock_monitor (self, monitor);
  }

  self->locked = TRUE;
  self->active_time = g_get_monotonic_time ();
  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_LOCKED]);
}


static void
phog_lockscreen_manager_set_property (GObject      *object,
                                       guint         property_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
  PhogLockscreenManager *self = PHOG_LOCKSCREEN_MANAGER (object);

  switch (property_id) {
  case PROP_LOCKED:
    phog_lockscreen_manager_set_locked (self, g_value_get_boolean (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
phog_lockscreen_manager_get_property (GObject    *object,
                                       guint       property_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
  PhogLockscreenManager *self = PHOG_LOCKSCREEN_MANAGER (object);

  switch (property_id) {
  case PROP_LOCKED:
    g_value_set_boolean (value, self->locked);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
phog_lockscreen_manager_dispose (GObject *object)
{
  PhogLockscreenManager *self = PHOG_LOCKSCREEN_MANAGER (object);

  g_clear_pointer (&self->shields, g_ptr_array_unref);
  g_clear_pointer (&self->lockscreen, phog_cp_widget_destroy);

  G_OBJECT_CLASS (phog_lockscreen_manager_parent_class)->dispose (object);
}


static void
phog_lockscreen_manager_constructed (GObject *object)
{
  G_OBJECT_CLASS (phog_lockscreen_manager_parent_class)->constructed (object);
}


static void
phog_lockscreen_manager_class_init (PhogLockscreenManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = phog_lockscreen_manager_constructed;
  object_class->dispose = phog_lockscreen_manager_dispose;

  object_class->set_property = phog_lockscreen_manager_set_property;
  object_class->get_property = phog_lockscreen_manager_get_property;

  props[PROP_LOCKED] =
    g_param_spec_boolean ("locked",
                          "Locked",
                          "Whether the screen is locked",
                          FALSE,
                          G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, PROP_LAST_PROP, props);

  /**
   * PhogLockscreenManager::wakeup-outputs
   * @self: The #PhogLockscreenManager emitting this signal
   *
   * Emitted when the outputs should be woken up.
   */
  signals[WAKEUP_OUTPUTS] = g_signal_new (
    "wakeup-outputs",
    G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
    NULL, G_TYPE_NONE, 0);
}


static void
phog_lockscreen_manager_init (PhogLockscreenManager *self)
{
}


PhogLockscreenManager *
phog_lockscreen_manager_new (void)
{
  return g_object_new (PHOG_TYPE_LOCKSCREEN_MANAGER, NULL);
}

/**
 * phog_lockscreen_set_locked:
 * @self: The #PhogLockscreenManager
 * @lock: %TRUE to lock %FALSE to unlock
 *
 * Lock or unlock the screen.
 */
void
phog_lockscreen_manager_set_locked (PhogLockscreenManager *self, gboolean lock)
{
  g_return_if_fail (PHOG_IS_LOCKSCREEN_MANAGER (self));
  if (lock == self->locked)
    return;

  if (lock)
    lockscreen_lock (self);
  else
    lockscreen_unlock_cb (self, PHOG_LOCKSCREEN (self->lockscreen));
}


gboolean
phog_lockscreen_manager_get_locked (PhogLockscreenManager *self)
{
  g_return_val_if_fail (PHOG_IS_LOCKSCREEN_MANAGER (self), FALSE);

  return self->locked;
}

/**
 * phog_lockscreen_manager_get_page
 * @self: The #PhogLockscreenManager
 *
 * Returns: The currently shown #PhogLockscreenPage in the #PhogLockscreen
 */
PhogLockscreenPage
phog_lockscreen_manager_get_page (PhogLockscreenManager *self)
{
  g_return_val_if_fail (PHOG_IS_LOCKSCREEN_MANAGER (self), FALSE);

  return phog_lockscreen_get_page (self->lockscreen);
}


gint64
phog_lockscreen_manager_get_active_time (PhogLockscreenManager *self)
{
  g_return_val_if_fail (PHOG_IS_LOCKSCREEN_MANAGER (self), 0);

  return self->active_time;
}


gboolean
phog_lockscreen_manager_set_page  (PhogLockscreenManager *self,
                                    PhogLockscreenPage     page)
{
  g_return_val_if_fail (PHOG_IS_LOCKSCREEN_MANAGER (self), FALSE);

  if (!self->lockscreen)
    return FALSE;

  g_return_val_if_fail (PHOG_IS_LOCKSCREEN (self->lockscreen), FALSE);

  phog_lockscreen_set_page (self->lockscreen, page);
  return TRUE;
}
