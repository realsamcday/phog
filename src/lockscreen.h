/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#pragma once

#include "layersurface.h"

G_BEGIN_DECLS

/**
 * PhogLockscreenPage:
 * @PHOG_LOCKSCREEN_PAGE_DEFAULT: The default locked page
 * @PHOG_LOCKSCREEN_PAGE_UNLOCK: The unlock page (where PIN is entered)
 *
 * This enum indicates which page is shown on the lockscreen.
 * This helps #PhogGnomeShellManager to decide when to emit
 * AcceleratorActivated events over DBus
 */
typedef enum {
  PHOG_LOCKSCREEN_PAGE_DEFAULT,
  PHOG_LOCKSCREEN_PAGE_UNLOCK,
} PhogLockscreenPage;

#define PHOG_TYPE_LOCKSCREEN (phog_lockscreen_get_type ())

G_DECLARE_FINAL_TYPE (PhogLockscreen, phog_lockscreen, PHOG, LOCKSCREEN,
                      PhogLayerSurface)

GtkWidget * phog_lockscreen_new (gpointer layer_shell, gpointer wl_output);
void        phog_lockscreen_set_page (PhogLockscreen *self, PhogLockscreenPage page);
PhogLockscreenPage phog_lockscreen_get_page (PhogLockscreen *self);

G_END_DECLS
