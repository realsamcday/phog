subdir('dbus')
subdir('monitor')
subdir('wwan')
subdir('settings')

phog_resources = gnome.compile_resources(
  'phog-resources',
  'phog.gresources.xml',

  c_name: 'phog',
)

phog_enum_headers = files(
  'drag-surface.h',
  'lockscreen.h',
  'monitor/monitor.h',
  'phog-wayland.h',
  'shell.h',
)

phog_enums = gnome.mkenums('phog-enums',
  h_template: 'phog-enums.h.in',
  c_template: 'phog-enums.c.in',
  sources: phog_enum_headers,
)

phog_settings_sources = files(
  'settings.c') + [
  phog_settings_widgets_sources,
]

phog_marshalers = gnome.genmarshal('phog-marshalers',
  sources : 'phog-marshalers.list',
  prefix : '_phog_marshal',
  valist_marshallers : true)

libphog_generated_sources = [
  phog_enums,
  phog_marshalers,
  phog_resources,
  wl_proto_sources,
  generated_dbus_sources,
]

phog_layer_surface_sources = files(
  'layersurface.c',
  'layersurface.h',
)

# Symbols from these availabel in tools and unit tests
libphog_tool_sources = files(
  'bidi.c',
  'bidi.h',
  'connectivity-info.c',
  'connectivity-info.h',
  'drag-surface.c',
  'drag-surface.h',
  'keypad.c',
  'keypad.h',
  'lockshield.c',
  'lockshield.h',
  'log.h',
  'log.c',
  'status-icon.c',
  'status-icon.h',
  'phog-wayland.c',
  'phog-wayland.h',
  'util.c',
  'util.h',
  'wl-buffer.c',
  'wl-buffer.h',
) + [
  libphog_generated_sources,
  phog_layer_surface_sources,
  phog_monitor_sources,
]

# Symbols from these are not availabel in tools and unit tests
libphog_sources = files(
  'arrow.c',
  'arrow.h',
  'batteryinfo.c',
  'batteryinfo.h',
  'fader.c',
  'fader.h',
  'keyboard-events.c',
  'keyboard-events.h',
  'idle-manager.c',
  'idle-manager.h',
  'lockscreen-manager.c',
  'lockscreen-manager.h',
  'lockscreen.c',
  'lockscreen.h',
  'monitor-manager.c',
  'monitor-manager.h',
  'osk-button.c',
  'osk-button.h',
  'osk-manager.c',
  'osk-manager.h',
  'top-panel.c',
  'top-panel.h',
  'shell.c',
  'shell.h',
  'toplevel-manager.c',
  'toplevel-manager.h',
  'toplevel.c',
  'toplevel.h',
  'wifiinfo.c',
  'wifiinfo.h',
  'wifimanager.c',
  'wifimanager.h',
  'wwan-info.c',
  'wwan-info.h',
  'greetd.c',
  'greetd.h',
  'greetd-session.c',
  'greetd-session.h',
) + [
  phog_settings_sources,
  phog_wwan_sources,
]

phog_deps = [
  fribidi_dep,
  gcr_dep,
  gio_dep,
  gio_unix_dep,
  glib_dep,
  gnome_desktop_dep,
  gobject_dep,
  gtk_dep,
  gtk_wayland_dep,
  gudev_dep,
  json_glib_dep,
  libhandy_dep,
  libnm_dep,
  libsystemd_dep,
  upower_glib_dep,
  wayland_client_dep,
  cc.find_library('m', required: false),
  cc.find_library('rt', required: false),
]

phog_inc = include_directories('.')
# A static library used by tests and tools
phog_tool_lib = static_library('phog-tool',
  libphog_tool_sources,
  include_directories: [root_inc, phog_inc],
  dependencies: phog_deps)
phog_tool_dep = declare_dependency(sources: libphog_generated_sources,
  include_directories: [root_inc, phog_inc],
  link_with: phog_tool_lib,
  dependencies: phog_deps)

phog_lib = both_libraries('phog',
  libphog_sources,
  include_directories: [root_inc, phog_inc],
  dependencies: [phog_tool_dep, phog_deps])

# Shared lib used by docs
phog_doc_dep = declare_dependency(sources: libphog_generated_sources,
  include_directories: [root_inc, phog_inc],
  link_with: phog_lib,
  dependencies: phog_deps)

# Static library used by shell and integration tests
phog_dep = declare_dependency(
  include_directories: [root_inc, phog_inc],
  link_with: phog_lib.get_static_lib(),
  dependencies: [phog_deps, phog_tool_dep])

phog = executable(meson.project_name(), 'main.c',
  dependencies: phog_dep,
  install: true,
  install_dir: libexecdir)
