/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */
#pragma once

#include "phog-enums.h"
#include "phog-wayland.h"

#include <gdk/gdk.h>
#include <glib-object.h>
#include <glib/gi18n.h>

G_BEGIN_DECLS

/**
 * PhogMonitorConnectorType:
 * @PHOG_MONITOR_CONNECTOR_TYPE_Unknown: unknown connector type
 * @PHOG_MONITOR_CONNECTOR_TYPE_VGA: a VGA connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_DVII: a DVII connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_DVID: a DVID connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_DVIA: a DVIA connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_Composite: a Composite connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_SVIDEO: a SVIDEO connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_LVDS: a LVDS connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_Component: a Component connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_9PinDIN: a 9PinDIN connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_DisplayPort: a DisplayPort connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_HDMIA: a HDMIA connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_HDMIB: a HDMIB connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_TV: a TV connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_eDP: a eDP connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_VIRTUAL: a Virtual connector
 * @PHOG_MONITOR_CONNECTOR_TYPE_DSI: a DSI connector
 *
 * This matches the values in drm_mode.h
 */
typedef enum _PhogMonitorConnectorType
{
  PHOG_MONITOR_CONNECTOR_TYPE_Unknown = 0,
  PHOG_MONITOR_CONNECTOR_TYPE_VGA = 1,
  PHOG_MONITOR_CONNECTOR_TYPE_DVII = 2,
  PHOG_MONITOR_CONNECTOR_TYPE_DVID = 3,
  PHOG_MONITOR_CONNECTOR_TYPE_DVIA = 4,
  PHOG_MONITOR_CONNECTOR_TYPE_Composite = 5,
  PHOG_MONITOR_CONNECTOR_TYPE_SVIDEO = 6,
  PHOG_MONITOR_CONNECTOR_TYPE_LVDS = 7,
  PHOG_MONITOR_CONNECTOR_TYPE_Component = 8,
  PHOG_MONITOR_CONNECTOR_TYPE_9PinDIN = 9,
  PHOG_MONITOR_CONNECTOR_TYPE_DisplayPort = 10,
  PHOG_MONITOR_CONNECTOR_TYPE_HDMIA = 11,
  PHOG_MONITOR_CONNECTOR_TYPE_HDMIB = 12,
  PHOG_MONITOR_CONNECTOR_TYPE_TV = 13,
  PHOG_MONITOR_CONNECTOR_TYPE_eDP = 14,
  PHOG_MONITOR_CONNECTOR_TYPE_VIRTUAL = 15,
  PHOG_MONITOR_CONNECTOR_TYPE_DSI = 16,
} PhogMonitorConnectorType;

/**
 * PhogMonitorTransform:
 * @PHOG_MONITOR_TRANSFORM_NORMAL: normal
 * @PHOG_MONITOR_TRANSFORM_90: 90 degree clockwise
 * @PHOG_MONITOR_TRANSFORM_180: 180 degree clockwise
 * @PHOG_MONITOR_TRANSFORM_270: 270 degree clockwise
 * @PHOG_MONITOR_TRANSFORM_FLIPPED: flipped clockwise
 * @PHOG_MONITOR_TRANSFORM_FLIPPED_90: flipped and 90 deg
 * @PHOG_MONITOR_TRANSFORM_FLIPPED_180: flipped and 180 deg
 * @PHOG_MONITOR_TRANSFORM_FLIPPED_270: flipped and 270 deg
 *
 * the monitors rotation. This corresponds to the values in
 * the org.gnome.Mutter.DisplayConfig DBus protocol.
 */
typedef enum _PhogMonitorTransform
{
  PHOG_MONITOR_TRANSFORM_NORMAL,
  PHOG_MONITOR_TRANSFORM_90,
  PHOG_MONITOR_TRANSFORM_180,
  PHOG_MONITOR_TRANSFORM_270,
  PHOG_MONITOR_TRANSFORM_FLIPPED,
  PHOG_MONITOR_TRANSFORM_FLIPPED_90,
  PHOG_MONITOR_TRANSFORM_FLIPPED_180,
  PHOG_MONITOR_TRANSFORM_FLIPPED_270,
} PhogMonitorTransform;


typedef struct _PhogMonitorMode
{
  int width, height;
  int refresh;
  guint32 flags;
} PhogMonitorMode;

/**
 * PhogMonitorPowerSaveMode:
 * @PHOG_MONITOR_POWER_SAVE_MODE_ON: The monitor is on
 * @PHOG_MONITOR_POWER_SAVE_MODE_OFF: The monitor is off (saving power)
 *
 * The power save mode of a monitor
 */
typedef enum _PhogMonitorPowerSaveMode {
  PHOG_MONITOR_POWER_SAVE_MODE_OFF = 0,
  PHOG_MONITOR_POWER_SAVE_MODE_ON  = 1,
} PhogMonitorPowerSaveMode;

#define PHOG_TYPE_MONITOR                 (phog_monitor_get_type ())

struct _PhogMonitor {
  GObject parent;

  struct wl_output *wl_output;
  struct zxdg_output_v1 *xdg_output;
  struct zwlr_output_power_v1 *wlr_output_power;
  PhogMonitorPowerSaveMode power_mode;

  int x, y, width, height;
  int subpixel;
  gint32 transform;

  struct {
    gint32 x, y, width, height;
  } logical;

  int width_mm;
  int height_mm;

  char *vendor;
  char *product;
  char *description;

  GArray *modes;
  guint current_mode;
  guint preferred_mode;

  char *name;
  PhogMonitorConnectorType conn_type;

  gboolean wl_output_done;
  gboolean xdg_output_done;

  struct zwlr_gamma_control_v1 *gamma_control;
  guint32 n_gamma_entries;
};

G_DECLARE_FINAL_TYPE (PhogMonitor, phog_monitor, PHOG, MONITOR, GObject)

PhogMonitor     * phog_monitor_new_from_wl_output (gpointer wl_output);
PhogMonitorMode * phog_monitor_get_current_mode (PhogMonitor *self);
gboolean           phog_monitor_is_configured (PhogMonitor *self);
gboolean           phog_monitor_is_builtin (PhogMonitor *self);
gboolean           phog_monitor_is_flipped (PhogMonitor *self);
gboolean           phog_monitor_has_gamma (PhogMonitor *self);
guint              phog_monitor_get_transform (PhogMonitor *self);
void               phog_monitor_set_power_save_mode (PhogMonitor *self,
                                                      PhogMonitorPowerSaveMode mode);
PhogMonitorPowerSaveMode phog_monitor_get_power_save_mode (PhogMonitor *self);
PhogMonitorConnectorType phog_monitor_connector_type_from_name (const char *name);
gboolean           phog_monitor_connector_is_builtin (PhogMonitorConnectorType type);
struct wl_output * phog_monitor_get_wl_output (PhogMonitor *self);
float              phog_monitor_get_fractional_scale (PhogMonitor *self);

gboolean           phog_monitor_transform_is_tilted (PhogMonitorTransform transform);

G_END_DECLS
