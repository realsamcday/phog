/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */
#pragma once

#include "ext-idle-notify-v1-client-protocol.h"
#include "virtual-keyboard-unstable-v1-client-protocol.h"
#include "wlr-foreign-toplevel-management-unstable-v1-client-protocol.h"
#include "wlr-input-inhibitor-unstable-v1-client-protocol.h"
#include "wlr-layer-shell-unstable-v1-client-protocol.h"
#include "wlr-gamma-control-unstable-v1-client-protocol.h"
#include "wlr-output-management-unstable-v1-client-protocol.h"
#include "wlr-output-power-management-unstable-v1-client-protocol.h"
#include "wlr-screencopy-unstable-v1-client-protocol.h"
#include "wlr-screencopy-unstable-v1-client-protocol.h"
#include "xdg-output-unstable-v1-client-protocol.h"
#include "xdg-shell-client-protocol.h"
#include "phoc-layer-shell-effects-unstable-v1-client-protocol.h"

/* This goes past the other wl protocols since it might need their structs */
#include "phosh-private-client-protocol.h"

#include <glib-object.h>

G_BEGIN_DECLS

/**
 * PhogWaylandSeatCapabilities:
 * @PHOG_WAYLAND_SEAT_CAPABILITY_NONE: no device detected
 * @PHOG_WAYLAND_SEAT_CAPABILITY_POINTER: the seat has pointer devices
 * @PHOG_WAYLAND_SEAT_CAPABILITY_KEYBOARD: the seat has one or more keyboards
 * @PHOG_WAYLAND_SEAT_CAPABILITY_TOUCH: the seat has touch devices
 *
 * These match wl_seat_capabilities
 */
typedef enum {
  PHOG_WAYLAND_SEAT_CAPABILITY_NONE     = 0,
  PHOG_WAYLAND_SEAT_CAPABILITY_POINTER  = (1 << 0),
  PHOG_WAYLAND_SEAT_CAPABILITY_KEYBOARD = (1 << 1),
  PHOG_WAYLAND_SEAT_CAPABILITY_TOUCH    = (1 << 2),
} PhogWaylandSeatCapabilities;

/* Versions of phosh-private protocol that add certain features */
#define PHOSH_PRIVATE_GET_THUMBNAIL_SINCE  4
#define PHOSH_PRIVATE_KBD_EVENTS_SINCE     5
#define PHOSH_PRIVATE_STARTUP_NOTIFY_SINCE 6
#define PHOSH_PRIVATE_SHELL_READY_SINCE    6

#define PHOG_TYPE_WAYLAND phog_wayland_get_type()

G_DECLARE_FINAL_TYPE (PhogWayland, phog_wayland, PHOG, WAYLAND, GObject)

PhogWayland                         *phog_wayland_get_default (void);
GHashTable                           *phog_wayland_get_wl_outputs (PhogWayland *self);
gboolean                              phog_wayland_has_wl_output  (PhogWayland *self,
                                                                    struct wl_output *wl_output);
struct ext_idle_notifier_v1          *phog_wayland_get_ext_idle_notifier_v1 (PhogWayland *self);
struct phosh_private                 *phog_wayland_get_phosh_private (PhogWayland *self);
uint32_t                              phog_wayland_get_phosh_private_version (PhogWayland *self);
struct wl_seat                       *phog_wayland_get_wl_seat (PhogWayland *self);
struct wl_shm                        *phog_wayland_get_wl_shm (PhogWayland *self);
struct xdg_wm_base                   *phog_wayland_get_xdg_wm_base (PhogWayland *self);
struct zwlr_foreign_toplevel_manager_v1 *phog_wayland_get_zwlr_foreign_toplevel_manager_v1 (PhogWayland *self);
struct zwlr_input_inhibit_manager_v1 *phog_wayland_get_zwlr_input_inhibit_manager_v1 (PhogWayland *self);
struct zwlr_layer_shell_v1           *phog_wayland_get_zwlr_layer_shell_v1 (PhogWayland *self);
struct zwlr_gamma_control_manager_v1 *phog_wayland_get_zwlr_gamma_control_manager_v1 (PhogWayland *self);
struct zwlr_output_manager_v1        *phog_wayland_get_zwlr_output_manager_v1 (PhogWayland *self);
struct zwlr_output_power_manager_v1 *phog_wayland_get_zwlr_output_power_manager_v1 (PhogWayland *self);
struct zxdg_output_manager_v1        *phog_wayland_get_zxdg_output_manager_v1 (PhogWayland *self);
struct zwlr_screencopy_manager_v1    *phog_wayland_get_zwlr_screencopy_manager_v1 (PhogWayland *self);
struct zwp_virtual_keyboard_manager_v1 *phog_wayland_get_zwp_virtual_keyboard_manager_v1 (PhogWayland *self);
void                                  phog_wayland_roundtrip (PhogWayland *self);
PhogWaylandSeatCapabilities          phog_wayland_get_seat_capabilities (PhogWayland *self);
struct zphoc_layer_shell_effects_v1  *phog_wayland_get_zphoc_layer_shell_effects_v1 (PhogWayland *self);

G_END_DECLS
