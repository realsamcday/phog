/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Julian Sparber <julian.sparber@puri.sm>
 */

#define G_LOG_DOMAIN "phog-status-icon"

#include "phog-config.h"

#include "status-icon.h"

/**
 * SECTION:status-icon
 * @short_description: Base clase for different status icons e.g in the top bar
 * @Title: PhogStatusIcon
 */

enum {
  PHOG_STATUS_ICON_PROP_0,
  PHOG_STATUS_ICON_PROP_ICON_NAME,
  PHOG_STATUS_ICON_PROP_ICON_SIZE,
  PHOG_STATUS_ICON_PROP_EXTRA_WIDGET,
  PHOG_STATUS_ICON_PROP_INFO,
  PHOG_STATUS_ICON_PROP_LAST_PROP
};
static GParamSpec *props[PHOG_STATUS_ICON_PROP_LAST_PROP];

typedef struct
{
  GtkWidget   *image;
  GtkWidget   *extra_widget;
  GtkIconSize  icon_size;
  char        *info;

  guint        idle_id;
} PhogStatusIconPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (PhogStatusIcon, phog_status_icon, GTK_TYPE_BIN);


static void
phog_status_icon_set_property (GObject *object,
                                  guint property_id,
                                  const GValue *value,
                                  GParamSpec *pspec)
{
  PhogStatusIcon *self = PHOG_STATUS_ICON (object);

  switch (property_id) {
  case PHOG_STATUS_ICON_PROP_ICON_NAME:
    phog_status_icon_set_icon_name (self, g_value_get_string (value));
    break;
  case PHOG_STATUS_ICON_PROP_ICON_SIZE:
    phog_status_icon_set_icon_size (self, g_value_get_enum (value));
    break;
  case PHOG_STATUS_ICON_PROP_EXTRA_WIDGET:
    phog_status_icon_set_extra_widget (self, g_value_get_object (value));
    break;
  case PHOG_STATUS_ICON_PROP_INFO:
    phog_status_icon_set_info (self, g_value_get_string (value));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}


static void
phog_status_icon_get_property (GObject *object,
                                  guint property_id,
                                  GValue *value,
                                  GParamSpec *pspec)
{
  PhogStatusIcon *self = PHOG_STATUS_ICON (object);

  switch (property_id) {
  case PHOG_STATUS_ICON_PROP_ICON_NAME:
    g_value_take_string (value, phog_status_icon_get_icon_name (self));
    break;
  case PHOG_STATUS_ICON_PROP_ICON_SIZE:
    g_value_set_enum (value, phog_status_icon_get_icon_size (self));
    break;
  case PHOG_STATUS_ICON_PROP_EXTRA_WIDGET:
    g_value_set_object (value, phog_status_icon_get_extra_widget (self));
    break;
  case PHOG_STATUS_ICON_PROP_INFO:
    g_value_set_string (value, phog_status_icon_get_info (self));
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}


static gboolean
on_idle (PhogStatusIcon *self)
{
  PhogStatusIconClass *klass = PHOG_STATUS_ICON_GET_CLASS (self);
  PhogStatusIconPrivate *priv = phog_status_icon_get_instance_private (self);

  if (klass->idle_init)
    (*klass->idle_init) (self);

  priv->idle_id = 0;
  return G_SOURCE_REMOVE;
}


static void
phog_status_icon_constructed (GObject *object)
{
  PhogStatusIcon *self = PHOG_STATUS_ICON (object);
  PhogStatusIconPrivate *priv = phog_status_icon_get_instance_private (self);
  PhogStatusIconClass *klass = PHOG_STATUS_ICON_GET_CLASS (self);

  G_OBJECT_CLASS (phog_status_icon_parent_class)->constructed (object);

  if (klass->idle_init)
    priv->idle_id = g_idle_add ((GSourceFunc) on_idle, self);
}


static void
phog_status_icon_dispose (GObject *object)
{
  PhogStatusIcon *self = PHOG_STATUS_ICON (object);
  PhogStatusIconPrivate *priv = phog_status_icon_get_instance_private (self);

  g_clear_handle_id (&priv->idle_id, g_source_remove);

  G_OBJECT_CLASS (phog_status_icon_parent_class)->dispose (object);
}


static void
phog_status_icon_finalize (GObject *gobject)
{
  PhogStatusIconPrivate *priv = phog_status_icon_get_instance_private (PHOG_STATUS_ICON (gobject));

  g_clear_pointer (&priv->info, g_free);

  G_OBJECT_CLASS (phog_status_icon_parent_class)->finalize (gobject);
}


static void
phog_status_icon_class_init (PhogStatusIconClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->set_property = phog_status_icon_set_property;
  object_class->get_property = phog_status_icon_get_property;
  object_class->constructed = phog_status_icon_constructed;
  object_class->dispose = phog_status_icon_dispose;
  object_class->finalize = phog_status_icon_finalize;

  gtk_widget_class_set_css_name (widget_class, "phog-status-icon");

  props[PHOG_STATUS_ICON_PROP_ICON_NAME] =
   g_param_spec_string ("icon-name",
                        "icon name",
                        "The icon name",
                        NULL,
                        G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY);

  props[PHOG_STATUS_ICON_PROP_ICON_SIZE] =
   g_param_spec_enum ("icon-size",
		      "icon size",
		      "The icon size",
		      GTK_TYPE_ICON_SIZE,
		      GTK_ICON_SIZE_LARGE_TOOLBAR,
		      G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY);

  props[PHOG_STATUS_ICON_PROP_EXTRA_WIDGET] =
   g_param_spec_object ("extra_widget",
			"extra widget",
			"An additional widget",
			GTK_TYPE_WIDGET,
			G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY);

  props[PHOG_STATUS_ICON_PROP_INFO] =
   g_param_spec_string ("info",
                        "info",
                        "Additional state information",
                        NULL,
                        G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY);

  g_object_class_install_properties (object_class, PHOG_STATUS_ICON_PROP_LAST_PROP, props);
}


static void
phog_status_icon_init (PhogStatusIcon *self)
{
  PhogStatusIconPrivate *priv = phog_status_icon_get_instance_private (self);
  GtkWidget *box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 6);

  gtk_widget_set_visible (box, TRUE);

  priv->icon_size = GTK_ICON_SIZE_LARGE_TOOLBAR;
  priv->image = gtk_image_new();
  gtk_widget_set_visible (priv->image, TRUE);

  gtk_container_add (GTK_CONTAINER (box), GTK_WIDGET (priv->image));

  if (priv->extra_widget) {
    gtk_container_add (GTK_CONTAINER (box), GTK_WIDGET (priv->extra_widget));
    gtk_widget_set_visible (priv->extra_widget, TRUE);
  }

  gtk_container_add (GTK_CONTAINER (self), box);
}


GtkWidget *
phog_status_icon_new (void)
{
  return g_object_new (PHOG_TYPE_STATUS_ICON, NULL);
}


void
phog_status_icon_set_icon_name (PhogStatusIcon *self, const char *icon_name)
{
  PhogStatusIconPrivate *priv;
  g_autofree char *old_icon_name = NULL;

  g_return_if_fail (PHOG_IS_STATUS_ICON (self));

  priv = phog_status_icon_get_instance_private (self);

  old_icon_name = phog_status_icon_get_icon_name (self);
  if (!g_strcmp0 (old_icon_name, icon_name))
    return;

  gtk_image_set_from_icon_name (GTK_IMAGE (priv->image),
				icon_name,
				phog_status_icon_get_icon_size (self));

  g_object_notify_by_pspec (G_OBJECT (self), props[PHOG_STATUS_ICON_PROP_ICON_NAME]);
}


char *
phog_status_icon_get_icon_name (PhogStatusIcon *self)
{
  PhogStatusIconPrivate *priv;
  char *icon_name;

  g_return_val_if_fail (PHOG_IS_STATUS_ICON (self), 0);

  priv = phog_status_icon_get_instance_private (self);

  g_object_get (priv->image, "icon-name", &icon_name, NULL);

  return icon_name;
}


void
phog_status_icon_set_icon_size (PhogStatusIcon *self, GtkIconSize size)
{
  PhogStatusIconPrivate *priv;
  g_return_if_fail (PHOG_IS_STATUS_ICON (self));

  priv = phog_status_icon_get_instance_private (self);

  if (priv->icon_size == size)
    return;

  priv->icon_size = size;
  g_object_set (priv->image, "icon-size", size, NULL);

  g_object_notify_by_pspec (G_OBJECT (self), props[PHOG_STATUS_ICON_PROP_ICON_SIZE]);
}


GtkIconSize
phog_status_icon_get_icon_size (PhogStatusIcon *self)
{
  PhogStatusIconPrivate *priv;

  g_return_val_if_fail (PHOG_IS_STATUS_ICON (self), 0);

  priv = phog_status_icon_get_instance_private (self);

  return priv->icon_size;
}


void
phog_status_icon_set_extra_widget (PhogStatusIcon *self, GtkWidget *widget)
{
  PhogStatusIconPrivate *priv;
  GtkWidget *box;
  g_return_if_fail (PHOG_IS_STATUS_ICON (self));

  priv = phog_status_icon_get_instance_private (self);
  box = gtk_bin_get_child (GTK_BIN (self));

  if (priv->extra_widget == widget)
    return;

  if (priv->extra_widget != NULL)
    gtk_container_remove (GTK_CONTAINER (box), priv->extra_widget);

  if (widget != NULL)
    gtk_container_add (GTK_CONTAINER (box), widget);

  priv->extra_widget = widget;

  g_object_notify_by_pspec (G_OBJECT (self), props[PHOG_STATUS_ICON_PROP_EXTRA_WIDGET]);
}


GtkWidget *
phog_status_icon_get_extra_widget (PhogStatusIcon *self)
{
  PhogStatusIconPrivate *priv;

  g_return_val_if_fail (PHOG_IS_STATUS_ICON (self), 0);

  priv = phog_status_icon_get_instance_private (self);

  return priv->extra_widget;
}


char *
phog_status_icon_get_info (PhogStatusIcon *self)
{
  PhogStatusIconPrivate *priv;

  g_return_val_if_fail (PHOG_IS_STATUS_ICON (self), 0);

  priv = phog_status_icon_get_instance_private (self);

  return priv->info;
}


void
phog_status_icon_set_info (PhogStatusIcon *self, const char *info)
{
  PhogStatusIconPrivate *priv;
  g_return_if_fail (PHOG_IS_STATUS_ICON (self));

  priv = phog_status_icon_get_instance_private (self);

  if (g_strcmp0 (priv->info, info) == 0)
    return;

  g_clear_pointer (&priv->info, g_free);
  priv->info = g_strdup (info);

  g_object_notify_by_pspec (G_OBJECT (self), props[PHOG_STATUS_ICON_PROP_INFO]);
}
