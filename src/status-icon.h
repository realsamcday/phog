/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define PHOG_TYPE_STATUS_ICON (phog_status_icon_get_type())

G_DECLARE_DERIVABLE_TYPE (PhogStatusIcon, phog_status_icon, PHOG, STATUS_ICON, GtkBin)

/**
 * PhogStatusIconClass:
 * @parent_class: The parent class
 * @idle_init: a callback to be invoked once on idle
 */
struct _PhogStatusIconClass
{
  GtkBinClass parent_class;

  void (*idle_init) (PhogStatusIcon *self);
};

GtkWidget * phog_status_icon_new (void);
void phog_status_icon_set_icon_size (PhogStatusIcon *self, GtkIconSize size);
GtkIconSize phog_status_icon_get_icon_size (PhogStatusIcon *self);
void phog_status_icon_set_icon_name (PhogStatusIcon *self, const char *icon_name);
char *phog_status_icon_get_icon_name (PhogStatusIcon *self);
void phog_status_icon_set_extra_widget (PhogStatusIcon *self, GtkWidget *widget);
GtkWidget * phog_status_icon_get_extra_widget (PhogStatusIcon *self);
void phog_status_icon_set_info (PhogStatusIcon *self, const char *info);
char *phog_status_icon_get_info (PhogStatusIcon *self);
void phog_status_icon_set_show_always (PhogStatusIcon *self, gboolean show_always);
gboolean phog_status_icon_get_show_always (PhogStatusIcon *self);

G_END_DECLS
