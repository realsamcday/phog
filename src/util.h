/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */
#pragma once

#include <gtk/gtk.h>
#include <gio/gdesktopappinfo.h>

#define STR_IS_NULL_OR_EMPTY(x) ((x) == NULL || (x)[0] == '\0')

#define phog_async_error_warn(err, ...) \
  phog_error_warnv (G_LOG_DOMAIN, err, G_IO_ERROR, G_IO_ERROR_CANCELLED, __VA_ARGS__)

#define phog_dbus_service_error_warn(err, ...) \
  phog_error_warnv (G_LOG_DOMAIN, err, G_IO_ERROR, G_IO_ERROR_NOT_FOUND, __VA_ARGS__)

void             phog_cp_widget_destroy (void *widget);
GDesktopAppInfo *phog_get_desktop_app_info_for_app_id (const char *app_id);
gchar           *phog_munge_app_id (const gchar *app_id);
char            *phog_strip_suffix_from_app_id (const char *app_id);
gboolean         phog_find_systemd_session (char **session_id);
gboolean         phog_error_warnv (const char  *log_domain,
                                    GError      *err,
                                    GQuark       domain,
                                    int          code,
                                    const gchar *fmt,
                                    ...) G_GNUC_PRINTF(5, 6);
int              phog_create_shm_file (off_t size);
char            *phog_util_escape_markup (const char *markup, gboolean allow_markup);
char            *phog_util_local_date (void);
gboolean         phog_util_gesture_is_touch (GtkGestureSingle *gesture);
gboolean         phog_util_have_gnome_software (gboolean scan);
void             phog_util_toggle_style_class (GtkWidget *widget, const char *style_class, gboolean toggle);
const char      *phog_util_get_stylesheet (const char *theme_name);
