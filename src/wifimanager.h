/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define PHOG_TYPE_WIFI_MANAGER (phog_wifi_manager_get_type())

G_DECLARE_FINAL_TYPE (PhogWifiManager, phog_wifi_manager, PHOG, WIFI_MANAGER, GObject)

PhogWifiManager  *phog_wifi_manager_new (void);
guint8             phog_wifi_manager_get_strength (PhogWifiManager *self);
const char        *phog_wifi_manager_get_icon_name (PhogWifiManager *self);
const char        *phog_wifi_manager_get_ssid (PhogWifiManager *self);
gboolean           phog_wifi_manager_get_enabled (PhogWifiManager *self);
void               phog_wifi_manager_set_enabled (PhogWifiManager *self, gboolean enabled);
gboolean           phog_wifi_manager_get_present (PhogWifiManager *self);

G_END_DECLS
